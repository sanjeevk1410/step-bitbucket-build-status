#!/bin/bash

set -e

build_state() {
  local state
  if [ -z "${WERCKER_RESULT}" ]; then
    state='INPROGRESS'
  else
    if [ "${WERCKER_RESULT}" = "passed" ]; then
      state='SUCCESSFUL'
    else
      state='FAILED'
    fi
  fi
  echo "${state}"
}

main() {
  local username=${WERCKER_BITBUCKET_BUILD_STATUS_USERNAME:?"username is required"}
  local password=${WERCKER_BITBUCKET_BUILD_STATUS_PASSWORD:?"password is required"}

  local state key name description url
  state="${WERCKER_BITBUCKET_BUILD_STATUS_BUILD_STATE:-$(build_state)}"
  key="${WERCKER_BITBUCKET_BUILD_STATUS_BUILD_KEY}"
  name="${WERCKER_BITBUCKET_BUILD_STATUS_BUILD_NAME}"
  description="${WERCKER_BITBUCKET_BUILD_STATUS_BUILD_DESCRIPTION}"
  url="${WERCKER_BITBUCKET_BUILD_STATUS_BUILD_URL:-${WERCKER_BUILD_URL}}"

  if [ "${state}" = "SUCCESSFUL" ]; then
    name="${WERCKER_BITBUCKET_BUILD_STATUS_SUCCESSFUL_NAME:-${name}}"
    description="${WERCKER_BITBUCKET_BUILD_STATUS_SUCCESSFUL_DESCRIPTION:-${description}}"
    url="${WERCKER_BITBUCKET_BUILD_STATUS_SUCCESSFUL_URL:-${url}}"
  elif [ "${state}" = "FAILED" ]; then
    name="${WERCKER_BITBUCKET_BUILD_STATUS_FAILED_NAME:-${name}}"
    description="${WERCKER_BITBUCKET_BUILD_STATUS_FAILED_DESCRIPTION:-${description}}"
    url="${WERCKER_BITBUCKET_BUILD_STATUS_FAILED_URL:-${url}}"
  fi

  WERCKER_STEP_TEMP=${WERCKER_STEP_TEMP:-$(mktemp -d)}

  local result
  result=$(curl \
    --data "key=${key}&state=${state}&name=${name}&url=${url}&description=${description}" \
    --user "${username}:${password}" \
    --output "${WERCKER_STEP_TEMP}/result.txt" \
    --write-out "%{http_code}" \
    --silent \
    "https://api.bitbucket.org/2.0/repositories/${WERCKER_GIT_OWNER}/${WERCKER_GIT_REPOSITORY}/commit/${WERCKER_GIT_COMMIT}/statuses/build"
  )

  if [ "${result}" != "200" ] && [ "${result}" != "201" ]; then
    fail "Error: $(cat "${WERCKER_STEP_TEMP}/result.txt")"
  fi
}

main
