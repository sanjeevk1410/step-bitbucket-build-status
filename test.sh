#!/bin/bash

set -e

dig_json() {
  local keys
  keys=$(echo "$*" | tr ' ' '\n' | awk '{keys=keys"[\""$1"\"]"}END{print keys}')
  python -c "import sys, json; print(json.load(sys.stdin)${keys})"
}

assert_build_state() {
  local actual expected
  expected=${1}
  actual=$(dig_json state < "${WERCKER_STEP_TEMP}/result.txt" )
  if [ "${actual}" != "${expected}" ]; then
    echo "test failed." 1>&2
    echo "expected: ${expected}" 1>&2
    echo "actual: ${actual}" 1>&2
    exit 1
  fi
}

WERCKER_BITBUCKET_BUILD_STATUS_USERNAME=${BITBUCKET_USER_NAME}
WERCKER_BITBUCKET_BUILD_STATUS_PASSWORD=${BITBUCKET_USER_PASSWORD}
WERCKER_BITBUCKET_BUILD_STATUS_BUILD_STATE=''
WERCKER_BITBUCKET_BUILD_STATUS_BUILD_KEY='WERCKER-STEP-BITBUCKET-BUILD-STATUS'
WERCKER_BITBUCKET_BUILD_STATUS_BUILD_NAME='Wercker CI'
WERCKER_BITBUCKET_BUILD_STATUS_BUILD_DESCRIPTION=''
WERCKER_BITBUCKET_BUILD_STATUS_BUILD_URL=''
WERCKER_BITBUCKET_BUILD_STATUS_SUCCESSFUL_NAME='Wercker CI passed'
WERCKER_BITBUCKET_BUILD_STATUS_SUCCESSFUL_DESCRIPTION='passed'
WERCKER_BITBUCKET_BUILD_STATUS_SUCCESSFUL_URL='https://bitbucket.org/odk211/step-bitbucket-build-status/successful'
WERCKER_BITBUCKET_BUILD_STATUS_FAILED_NAME='Wercker CI failed'
WERCKER_BITBUCKET_BUILD_STATUS_FAILED_DESCRIPTION='failed'
WERCKER_BITBUCKET_BUILD_STATUS_FAILED_URL='https://bitbucket.org/odk211/step-bitbucket-build-status/failed'

# for local test
# WERCKER_GIT_OWNER=odk211
# WERCKER_GIT_REPOSITORY=step-bitbucket-build-status
# WERCKER_GIT_COMMIT=97c50d9d67e28d290285213c2ca66633f218f4e6
# WERCKER_BUILD_URL=https://bitbucket.org/odk211/step-bitbucket-build-status

unset WERCKER_RESULT
. ./run.sh
assert_build_state INPROGRESS

WERCKER_RESULT=failed
main
assert_build_state FAILED

WERCKER_RESULT=passed
main
assert_build_state SUCCESSFUL
